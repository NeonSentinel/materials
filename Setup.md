<small>Version 1.0 - Mon Jan 13 16:05:15 UTC 2020</small>


# Setup


## A: Create a GitLab account

1. (Optional) Create an obfuscated email.
2. Create a GitLab account.
3. Create an issue on this project, using the 'Authorization Request' description template.
4. Wait for an email from GitLab granting you access.
5. Find your class group on GitLab (https://gitlab.com/wne-csit/cs-220/[SEMESTER]/class ; replace [SEMESTER] with something like spring-2020).


## B: Roll Call

1. Return to your class group on GitLab
2. Open the board for this group (see the issues menu in the left navigation).
3. Open the Roll Call issue.
4. Leave a comment on the issue.


## C: Read the syllabus

1. Read the syllabus.
2. Talk about it with at least one other person.
3. Write questions in the "Review Syllabus" issue.


<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
